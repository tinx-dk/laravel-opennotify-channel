## Contents

- [Installation](#installation)
    - [Setting up your OpenNotify account](#setting-up-your-opennotify-account)
- [Usage](#usage)

## Installation

You can install the package via composer:

```bash
composer config repositories.tinx-dk composer https://gitlab.com/api/v4/group/tinx-dk/-/packages/composer/
composer require "tinx-dk/laravel-opennotify-channel:^0.2.4"
```

### Setting up your OpenNotify account

Before you can use OpenNotify you have to
1) [create a system](https://app.opennotify.dk/admin/systems) in the OpenNotify administration. 
2) create a token in the system, by editing the system you created.
3) copy the token to the services config file:
```php
// config/services.php
return [
// ...
  'opennotify' => [
      'token' => 'YOUR_SYSTEM_TOKEN',
  ],
// ...
];
```

## Usage

Now you can use the channel in your `via()` method inside the notification as well as send a notification:

``` php
use TINXDK\OpenNotify\OpenNotifyChannel;
use TINXDK\OpenNotify\OpenNotifyMessage;
use Illuminate\Notifications\Notification;

class AccountApproved extends Notification
{
    public function via($notifiable)
    {
        return ['open_notify'];
    }

    public function toOpenNotify($notifiable)
    {
        return OpenNotifyMessage::create()
            ->text('The invoice has been paid.')
            ->subject('Invoice paid')
            ->addChannel('sms')
            ->addChannel('email');
    }
}
```
Only add the channels you want the notification to be sent on.


Make sure there is a `routeNotificationForOpenNotify` method on your notifiable model, for instance:
``` php
...
public function routeNotificationForOpenNotify()
{
    return \TINXDK\OpenNotify\OpenNotifyRecipient::create()
        ->email($this->email)
        ->mobile($this->mobile);
}
```
Remember that if you can allow the user to control some preferences and have settings to allow email or mobile, and then only OpenNotifyRecipient append the email or mobile if the user wants to be notified on those channels.
