<?php

namespace TINXDK\OpenNotify;

use GuzzleHttp\Client as HttpClient;
use Illuminate\Notifications\ChannelManager;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\ServiceProvider;

/**
 * Class OpenNotifyChannelServiceProvider
 * @package TINXDK\OpenNotify
 * @codeCoverageIgnore
 */
class OpenNotifyChannelServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        Notification::resolved(function (ChannelManager $service) {
            $service->extend('open_notify', function ($app) {
                return new OpenNotifyChannel($app->make(HttpClient::class),
                    env('OPENNOTIFY_API', 'https://app.opennotify.dk/api'),
                    config('services.opennotify.token', '')
                );
            });
        });
    }
}
