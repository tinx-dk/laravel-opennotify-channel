<?php


namespace TINXDK\OpenNotify;

/**
 * Interface OpenNotifyMessageInterface
 * @package TINXDK\OpenNotify
 */
interface OpenNotifyMessageInterface
{
    public function toArray(): array;
}