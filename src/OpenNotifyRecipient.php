<?php


namespace TINXDK\OpenNotify;


class OpenNotifyRecipient
{
    protected array $data;

    /**
     * @param array $data
     * @return OpenNotifyRecipient
     */
    public static function create(array $data = []): OpenNotifyRecipient
    {
        return new static($data);
    }

    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    /**
     * @param string $email
     * @return OpenNotifyRecipient
     */
    public function email(string $email): OpenNotifyRecipient
    {
        $this->data['email'] = trim($email);
        return $this;
    }

    public function hasEmail(): bool
    {
        return !empty($this->data['email']);
    }

    /**
     * @param string $mobile
     * @param string $defaultCountryCode
     * @return OpenNotifyRecipient
     */
    public function mobile(string $mobile, string $defaultCountryCode = "+45"): OpenNotifyRecipient
    {
        $mobile = trim($mobile);
        if (empty($mobile)) {
            unset($this->data['mobile']);
            return $this;
        }
        // Remove any character from the mobile that is not a + sign or number
        $mobile = preg_replace("/[^+0-9]/", "", trim($mobile));
        if ($mobile == "") {
            unset($this->data['mobile']);
            return $this;
        }
        if (substr($mobile, 0, 2) === "00") {
            $mobile = "+" . substr($mobile, 2);
        }
        elseif (substr($mobile, 0, 1) !== "+") {
            $mobile = $defaultCountryCode . $mobile;
        }
        $this->data['mobile'] = $mobile;
        return $this;
    }

    public function hasMobile(): bool
    {
        return !empty($this->data['mobile']);
    }

    public function toArray(): array
    {
        if (isset($this->data['mobile'])) {
            $this->mobile($this->data['mobile']);
        }
        return $this->data;
    }
}