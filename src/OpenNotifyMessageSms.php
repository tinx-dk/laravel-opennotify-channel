<?php

namespace TINXDK\OpenNotify;

class OpenNotifyMessageSms implements OpenNotifyMessageInterface
{

    protected string $text;

    /**
     * @param string $text
     * @return OpenNotifyMessageSms
     */
    public function text(string $text): OpenNotifyMessageSms
    {
        $this->text = $text;
        return $this;
    }

    public function toArray(): array
    {
        return ['text' => $this->text];
    }
}