<?php

namespace TINXDK\OpenNotify;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Illuminate\Notifications\Notification;
use Psr\Http\Message\ResponseInterface;

class OpenNotifyChannel
{

    /**
     * The HTTP client instance.
     *
     * @var Client
     */
    protected $http;

    protected string $token = "";
    protected string $api = "";

    /**
     * Create a new Slack channel instance.
     *
     * @param Client $http
     * @param string $api
     * @param string $token
     */
    public function __construct(Client $http, string $api, string $token)
    {
        $this->http = $http;
        $this->token = $token;
        $this->api = $api;
    }

    /**
     * Send the given notification.
     *
     * @param  mixed  $notifiable
     * @param Notification $notification
     * @return ResponseInterface|null
     */
    public function send($notifiable, Notification $notification)
    {
        /** @var OpenNotifyMessage $message */
        $message = $notification->toOpenNotify($notifiable);
        /** @var OpenNotifyRecipient $recipient */
        $recipient = $notifiable->routeNotificationForOpenNotify();
        if ($recipient->hasMobile() || $recipient->hasEmail()) {
            $message->addRecipient($recipient);
            return $this->http->post($this->api . '/notification', [
                RequestOptions::HEADERS => [
                    'Authorization' => 'Bearer ' . $this->token,
                    'Accept' => 'application/json',
                ],
                RequestOptions::JSON => $this->buildJsonPayload($message),
            ]);
        }
    }

    /**
     * Build up a JSON payload for the Open Notify API.
     *
     * @param OpenNotifyMessage $message
     * @return array
     */
    protected function buildJsonPayload(OpenNotifyMessage $message)
    {
        return array_filter([
            'channels' => $message->channels,
            'recipients' => $message->recipients,
        ]);
    }
}
