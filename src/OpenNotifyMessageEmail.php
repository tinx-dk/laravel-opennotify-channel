<?php

namespace TINXDK\OpenNotify;

class OpenNotifyMessageEmail implements OpenNotifyMessageInterface
{

    protected string $subject;
    protected string $text;
    protected string $html;

    /**
     * @param string $subject
     * @return OpenNotifyMessageEmail
     */
    public function subject(string $subject): OpenNotifyMessageEmail
    {
        $this->subject = $subject;
        return $this;
    }

    /**
     * @param string $html
     * @return OpenNotifyMessageEmail
     */
    public function html(string $html): OpenNotifyMessageEmail
    {
        $this->html = $html;
        return $this;
    }

    /**
     * @param string $text
     * @return OpenNotifyMessageEmail
     */
    public function text(string $text): OpenNotifyMessageEmail
    {
        $this->text = $text;
        return $this;
    }

    public function toArray(): array
    {
        return [
            'subject' => $this->subject,
            'text' => $this->text,
            'html' => $this->html,
        ];
    }
}