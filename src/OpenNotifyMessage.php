<?php

namespace TINXDK\OpenNotify;

class OpenNotifyMessage
{
    public array $channels = [];
    public array $recipients = [];

    /**
     * @return static
     */
    public static function create(): OpenNotifyMessage
    {
        return new static();
    }

    /**
     * @param array $channels
     * @return OpenNotifyMessage
     */
    public function setChannels(array $channels): OpenNotifyMessage
    {
        $this->channels = $channels;
        return $this;
    }

    /**
     * @param string $channel
     * @param OpenNotifyMessageInterface $data
     * @return OpenNotifyMessage
     */
    public function addChannel(string $channel, OpenNotifyMessageInterface $data): OpenNotifyMessage
    {
        $this->channels[$channel] = $data->toArray();
        return $this;
    }

    /**
     * @param array $recipients
     * @return OpenNotifyMessage
     */
    public function setRecipients(array $recipients): OpenNotifyMessage
    {
        $this->recipients = $recipients;
        return $this;
    }

    /**
     * @param OpenNotifyRecipient $recipient
     * @return OpenNotifyMessage
     */
    public function addRecipient(OpenNotifyRecipient $recipient): OpenNotifyMessage
    {
        $this->recipients[] = $recipient->toArray();
        return $this;
    }
}