<?php


namespace TINXDK\Tests\OpenNotify;


use TINXDK\OpenNotify\OpenNotifyRecipient;

class RecipientMobileVerificationTest extends \PHPUnit\Framework\TestCase
{

    public function provideNumbers()
    {
        return [
            ['+45 1234 5678', '+4512345678'],
            ['+45 1234-5678', '+4512345678'],
            ['0045 1234-5678', '+4512345678'],
            ['12345678', '+4512345678'],
            ['1234 5678', '+4512345678'],
            ['1234-56sd78', '+4512345678'],
            ['string', null],
            ['', null],
        ];
    }

    /**
     * @param $actual
     * @param $expected
     * @dataProvider provideNumbers
     * @covers \TINXDK\OpenNotify\OpenNotifyRecipient
     */
    public function testMobileVerification($actual, $expected)
    {
        $r = OpenNotifyRecipient::create(['mobile' => $actual]);
        $this->assertFalse($r->hasEmail());
        if ($expected === null) {
            $this->assertArrayNotHasKey('mobile', $r->toArray());
            $this->assertFalse($r->hasMobile());
        }
        else {
            $this->assertEquals($expected, $r->toArray()['mobile']);
            $this->assertTrue($r->hasMobile());
        }
    }
}