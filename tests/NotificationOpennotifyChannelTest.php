<?php

namespace TINXDK\Tests\OpenNotify;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\RequestOptions;
use Illuminate\Notifications\Notifiable;
use Illuminate\Notifications\Notification;
use Mockery as m;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use PHPUnit\Framework\TestCase;
use TINXDK\OpenNotify\OpenNotifyChannel;
use TINXDK\OpenNotify\OpenNotifyMessage;
use TINXDK\OpenNotify\OpenNotifyMessageEmail;
use TINXDK\OpenNotify\OpenNotifyMessageSms;
use TINXDK\OpenNotify\OpenNotifyRecipient;

/**
 * @covers \TINXDK\OpenNotify\OpenNotifyChannel
 * @covers \TINXDK\OpenNotify\OpenNotifyMessage
 * @covers \TINXDK\OpenNotify\OpenNotifyMessageSms
 * @covers \TINXDK\OpenNotify\OpenNotifyMessageEmail
 * @covers \TINXDK\OpenNotify\OpenNotifyRecipient
 * @uses \TINXDK\OpenNotify\OpenNotifyMessageInterface
 */
class NotificationOpennotifyChannelTest extends TestCase
{
    use MockeryPHPUnitIntegration;

    private OpenNotifyChannel $channel;

    /**
     * @var \Mockery\MockInterface|\GuzzleHttp\Client
     */
    private $guzzleHttp;

    protected function setUp(): void
    {
        parent::setUp();

        $this->guzzleHttp = m::mock(Client::class);

        $this->channel = new OpenNotifyChannel($this->guzzleHttp, 'api_url', 'testToken');
    }

    public function testNoMobile()
    {
        $payload = [
            RequestOptions::HEADERS => [
                'Authorization' => 'Bearer testToken',
                'Accept' => 'application/json',
            ],
            RequestOptions::JSON => [
                'recipients' => [
                    ['email' => 'test2@tinx.dk']
                ],
                'channels' => [
                    'email' => [
                        'subject' => 'TESTER',
                        'text' => 'This is only a test email',
                        'html' => '<h1>THIS IS A TEST</h1>',
                    ],
                    'sms' => [
                        'text' => 'This is only a test sms',
                    ],
                ],
            ]
        ];

        $this->guzzleHttp->shouldReceive('post')->andReturnUsing(function ($argUrl, $argPayload) use ($payload) {
            $this->assertEquals('api_url/notification', $argUrl);
            $this->assertEquals($payload, $argPayload);

            return new Response();
        });

        $this->channel->send(new NotificationTestNoMobileNotifiable(), new NotificationTestNotification());
    }

    public function testNoRecipients()
    {
        $this->guzzleHttp->shouldNotReceive('post');
        $this->channel->send(new NotificationTestNotNotifiable(), new NotificationTestNotification());
    }
}

class NotificationTestNotifiable
{
    use Notifiable;

    public function routeNotificationForOpenNotify(): OpenNotifyRecipient
    {
        return OpenNotifyRecipient::create()->email('test@tinx.dk')->mobile('+01245364');
    }
}

class NotificationTestNotNotifiable
{
    use Notifiable;

    public function routeNotificationForOpenNotify(): OpenNotifyRecipient
    {
        return OpenNotifyRecipient::create()->email('')->mobile('');
    }
}

class NotificationTestNoMobileNotifiable
{
    use Notifiable;

    public function routeNotificationForOpenNotify(): OpenNotifyRecipient
    {
        return OpenNotifyRecipient::create()->email('test2@tinx.dk')->mobile('');
    }
}

class NotificationTestNotification extends Notification
{
    public function toOpenNotify($notifiable): OpenNotifyMessage
    {
        $message = OpenNotifyMessage::create()
            ->setChannels(['email' =>
                (new OpenNotifyMessageEmail())
                    ->subject('TESTER')
                    ->text('This is only a test email')
                    ->html('<h1>THIS IS A TEST</h1>')
                    ->toArray()
            ]);
        $message->addChannel('sms', (new OpenNotifyMessageSms())->text('This is only a test sms'));
        return $message;
    }
}
